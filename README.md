# Challenge - Unity

## REQUISITOS
- O projeto deve ser desenvolvido em Unity3D versão 2019.2.17f1 ou posterior (Free Personal License).

- Deve ser usado apenas recursos do UnityEngine ou nativos C# (Não é permitido o uso de bibliotecas, scripts e/ou assets que não pertençam ao Unity).

- O projeto deve ter um controle de versão em um repositório Git (GitHub ou BitBucket) e o link do projeto deve ser enviado para jobs@pushstart.com.br

- Disponibilizar um .apk do jogo para teste é um diferencial.

- O prazo de entrega é até o dia 01/fevereiro/2020.

**ESTE TESTE É DIVIDIDO POR ENTREGAS.  A ENTREGA ESSENCIAL QUE É OBRIGATÓRIA E OS DESAFIOS BÔNUS QUE SÃO OPCIONAIS. VOCÊ PODE ESCOLHER REALIZAR OU NÃO OS DESAFIOS BÔNUS.**

**O CANDIDATO É LIVRE PARA INCLUIR MELHORIAS DE USABILIDADE OU MECÂNICAS QUE MELHOREM A EXPERIÊNCIA.**

## ENTREGA ESSENCIAL

Nessa entrega o jogador entra na cena de administração de uma pequena cidade. Nessa cena do jogo o jogador vê no canto superior esquerdo da tela o seu nickname, a quantia de dinheiro que possui e um indicador de porcentagem de felicidade da população que começa em 0%
Na parte inferior da tela o jogador pode escolher entre 5 tipos de construções diferentes: Casas, Fazenda, Fábrica, Shopping e Parque Natural. 

Ao arrastar uma construção partindo desse menu na parte inferior para o centro da tela e soltar a construção será iniciada, se possível. 

Construções quando iniciadas demoram um tempo (que varia entre cada tipo de construção) para ficarem prontas e quando ficam começam a render dinheiro. 

Quando estiver sem dinheiro suficiente para algumas construções, os botões dessas construções ficam desabilitados impedindo o jogador de arrastar novas construções para a cena. 

Cada construção tem sua frequência de rendimento e, toda vez que uma construção rende alguma quantia de dinheiro, sprites de moedas saem da posição da construção e se movem até a posição na HUD onde é mostrada a quantidade de dinheiro do jogador.

É possível rolar pela tela realizando o gesto de swipe e também pausar o jogo apertando um botão no canto direito superior.

O objetivo do jogo é **atingir 100% de felicidade da população**.

**REGRAS DA ECONOMIA DO JOGO**
- Casas geram 5 moedas por segundo.

- Fábricas geram 20 moedas por segundo.

- Shoppings geram 20 moedas por segundo.

- Parques geram 5 moedas por segundo.

- Fazendas geram 18 moedas por segundo.

**REGRAS DO MEDIDOR DE FELICIDADE DA POPULAÇÃO**
O jogador ganha 0.4% de felicidade da população por segundo, até atingir o máximo possível de felicidade da população.
Cada casa aumenta o máximo de porcentagem em 5%. Por exemplo: Se o jogador tem 5 casas, o máximo possível é de 25%

## REGRAS DO TESTE
- Utilizar os componentes de UI oficiais do Unity para criar o contador de dinheiro e os botões de cada construção

- Construções criadas devem estar posicionadas no mundo (World Space)

- Não utilizar nenhum asset adicional de terceiros.

## ASSETS
Os assets para usar estão em http://shared.pushstart.com.br/testes/unity/assets.zip

## DESAFIO BÔNUS 1: THE SCALE UPDATE
Crie uma cena de seleção de fase com 5 opções diferentes: Cada opção deve fazer o jogador começar com uma quantidade diferente de dinheiro, os preços das construções devem ser diferentes e o jogador deve começar com algumas construções já prontas no mapa em posições diferentes em cada fase

## DESAFIO BÔNUS 2: THE DEPTH UPDATE
Implemente as regras adicionais de economia e de medidor de felicidade da população
- Se a quantidade de casas for maior que o dobro da quantidade de fábricas, cada casa rende 1 moeda a menos para cada casa excedente dessa diferença. Por exemplo: Se houver 11 casas e 4 fábricas, cada casa vai passar a gerar 7 moedas.

- Se a quantidade de casas for menor que o triplo da quantidade de shoppings, cada shopping rende 2 moedas a menos para cada casa excedente dessa diferença.

- Se a quantidade de casas for menor que o dobro da quantidade de fazendas, cada fazenda rende 3 moedas a menos para cada casa excedente dessa diferença

- Se a quantidade de fábricas for maior que 150% da quantidade de parques naturais, o jogador começa a perder 0.25% de felicidade da população por segundo

- Se a quantidade de casas for maior que o dobro da quantidade de fábricas e fazendas somadas,  o jogador começa a perder 0.3% de felicidade da população por segundo

- Se a quantidade de shoppings for menor que 20% da quantidade de casas, o jogador começa a perder 6% de felicidade da população por minuto






